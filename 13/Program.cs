﻿using System;
using System.IO;
using System.Numerics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _13
{
    static class Program
    {
        static string lFile = @"..\..\files\l.txt";
        static string neFile = @"..\..\files\(n, e).txt";
        static string pAndQFile = @"..\..\files\p and q.txt";
        static string dFile = @"..\..\files\d.txt";
        static string phiNFile = @"..\..\files\phi n.txt";
        static string pqsFile = @"..\..\files\pqs.txt";
        static string pqoFile = @"..\..\files\pqo.txt";

        static void Main(string[] args)
        {
            //GetRSAOptions();
            //GetPQbyPhiN();
            GetPQbyOptions();
        }

        static void GetRSAOptions()
        {
            Random rand = new Random();

            int l = int.Parse(File.ReadAllLines(lFile)[0]);

            BigInteger p = GenPrime(l, rand);
            Console.WriteLine(" p = " + p);
            BigInteger q = GenPrime(l, rand);
            Console.WriteLine(" q = " + q);
            File.WriteAllText(pAndQFile, p + "\n" + q);

            BigInteger n = p * q;
            Console.WriteLine(" n = " + n);

            BigInteger phiN = (p - 1) * (q - 1);
            Console.WriteLine(" Phi(n) = " + phiN);
            File.WriteAllText(phiNFile, phiN.ToString());


            BigInteger e = 1;
            while (e == 1)
            {
                string tmp = "";
                for (int i = 0; i < phiN.ToString().Length; i++)
                {
                    tmp += rand.Next() % 10;
                }
                e = BigInteger.Parse(tmp) % phiN;
            }

            while (BigInteger.GreatestCommonDivisor(phiN, e) != 1)
            {
                e = (++e) % phiN;
                if (e == 1) e++;
            }

            Console.WriteLine(" e = " + e);
            File.WriteAllText(neFile, n + "\n" + e);

            BigInteger d = (Extended_GCD(phiN, e)[2] + phiN) % phiN;
            Console.WriteLine(" d = " + d);
            File.WriteAllText(dFile, d.ToString());

        }

        static void GetPQbyPhiN()
        {
            BigInteger n = BigInteger.Parse(File.ReadAllLines(neFile)[0]);
            BigInteger phiN = BigInteger.Parse(File.ReadAllLines(phiNFile)[0]);

            BigInteger a = 1;
            BigInteger b = -(n - phiN + 1);
            BigInteger c = n;

            BigInteger D = BigInteger.Pow(b, 2) - 4 * a * c;
            if (D > 0 || D == 0)
            {
                BigInteger x1 = ((-b - Sqrt(D)) / (2 * a) + phiN) % phiN;
                BigInteger x2 = ((-b + Sqrt(D)) / (2 * a) + phiN) % phiN;
                Console.WriteLine(" p = {0}\n q = {1}", x1, x2);

                File.WriteAllText(pqsFile, x1 + "\n" + x2);
            }
        }

        static void GetPQbyOptions()
        {
            Random rand = new Random();

            BigInteger n = BigInteger.Parse(File.ReadAllLines(neFile)[0]);
            BigInteger e = BigInteger.Parse(File.ReadAllLines(neFile)[1]);
            BigInteger d = BigInteger.Parse(File.ReadAllLines(dFile)[0]);

            BigInteger edm1 = e * d - 1;

            int f = 0;

            BigInteger s = edm1;
            while (s % 2 == 0)
            {
                s /= 2;
                f++;
            }

            BigInteger a = 1;
            BigInteger u = -1;
            BigInteger v = 0;
            while (u == -1)
            {
                a = 1;
                while (a <= 3)
                {
                    string tmp = "";
                    for (int i = 0; i < (n - 2).ToString().Length; i++)
                    {
                        tmp += rand.Next() % 10;
                    }
                    a = BigInteger.Parse(tmp) % (n - 2);
                }

                u = BigInteger.ModPow(a, s, n);
                v = BigInteger.ModPow(u, 2, n);

                while (v != 1)
                {
                    u = v;
                    v = BigInteger.ModPow(u, 2, n);
                }
            }

            BigInteger p = BigInteger.GreatestCommonDivisor(u - 1, n);
            Console.WriteLine(" p = " + p);
            BigInteger q = BigInteger.GreatestCommonDivisor(u + 1, n);
            Console.WriteLine(" q = " + q);
            
            File.WriteAllText(pqoFile, p + "\n" + q);
        }


        static BigInteger GenPrime(int n, Random rand)
        {
            BigInteger p = 0;
            string str = ((rand.Next() % 9) + 1) + "";
            for (int i = 1; i < n; i++)
            {
                str += rand.Next() % 10;
            }
            p = BigInteger.Parse(str);

            if (p % 2 == 0) { p -= 1; }

            while (!IsPrime(p))
            {
                p += 2;

                if (p.ToString().Length > n)
                {
                    str = "1";
                    for (int i = 2; i < n; i++)
                    {
                        str += "0";
                    }
                    str += "1";
                    p = BigInteger.Parse(str);
                }
            }

            return p;
        }

        static bool IsPrime(BigInteger p)
        {
            BigInteger p1 = p - 1;

            int s = 0;

            BigInteger t = p1;
            while (t % 2 == 0)
            {
                t /= 2;
                s++;
            }

            if (s == 0)
            {
                return false;
            }

            for (int i = 0; i < 30; i++)
            {
                Random rand = new Random();
                BigInteger tmp = 0;
                while (tmp > p1 || tmp < 2)
                {
                    string str = "";
                    for (int j = 0; j < p1.ToString().Length; j++)
                        str += rand.Next() % 10;

                    tmp = BigInteger.Parse(str);
                    tmp %= p;
                }

                BigInteger x = BigInteger.ModPow(tmp, t, p);

                if (x <= 1 || x == p1)
                {
                    continue;
                }

                bool f = false;
                for (int j = 0; j < s - 1; j++)
                {
                    x = BigInteger.ModPow(x, 2, p);

                    if (x == 1)
                    {
                        return false;
                    }

                    if (x == p1)
                    {
                        f = true;
                        break;
                    }
                }

                if (!f)
                {
                    return false;
                }
            }
            return true;
        }

        static BigInteger[] Extended_GCD(BigInteger A, BigInteger B)
        {
            BigInteger[] result = new BigInteger[3];
            bool reverse = false;
            if (A < B) //if A less than B, switch them
            {
                BigInteger temp = A;
                A = B;
                B = temp;
                reverse = true;
            }
            BigInteger r = B;
            BigInteger q = 0;
            BigInteger x0 = 1;
            BigInteger y0 = 0;
            BigInteger x1 = 0;
            BigInteger y1 = 1;
            BigInteger x = 0, y = 0;
            while (A % B != 0)
            {
                r = A % B;
                q = A / B;
                x = x0 - q * x1;
                y = y0 - q * y1;
                x0 = x1;
                y0 = y1;
                x1 = x;
                y1 = y;
                A = B;
                B = r;
            }
            result[0] = r;
            if (reverse)
            {
                result[1] = y;
                result[2] = x;
            }
            else
            {
                result[1] = x;
                result[2] = y;
            }
            return result;
        }

        static BigInteger Sqrt(this BigInteger n)
        {
            if (n == 0) return 0;
            if (n > 0)
            {
                int bitLength = Convert.ToInt32(Math.Ceiling(BigInteger.Log(n, 2)));
                BigInteger root = BigInteger.One << (bitLength / 2);

                while (!isSqrt(n, root))
                {
                    root += n / root;
                    root /= 2;
                }

                return root;
            }

            throw new ArithmeticException("NaN");
        }

        static bool isSqrt(BigInteger n, BigInteger root)
        {
            BigInteger lowerBound = root * root;
            BigInteger upperBound = (root + 1) * (root + 1);

            return (n >= lowerBound && n < upperBound);
        }

    }
}
