﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3
{
    class Program
    {
        static string textFile = @"..\..\files\text.txt";
        static string sampleTextFile = @"..\..\files\sample_text.txt";
        static string alphabetFile = @"..\..\files\alphabet.txt";
        static string forbiddenBigramsFile = @"..\..\files\forbidden_bigrams.txt";
        static string keyFile = @"..\..\files\key.txt";
        static string ciphertextFile = @"..\..\files\ciphertext.txt";
        static string decryptedTextFile = @"..\..\files\decrypted_text.txt";
        static string tableFile = @"..\..\files\table.txt";
        static string forestFile = @"..\..\files\forest.txt";

        static int keyLength = 7;

        static void Main(string[] args)
        {
            Console.WriteLine("1 - Генерация ключа;\n2 - Шифрование текста;\n3 - Ввод длины ключа;\n4 - Построение запрещенных биграмм;\n5 - Построение вспомогательной таблицы;\n6 - Построение леса;\n7 - Проверка ключей.\n");

            while (true)
            {
                string command = Console.ReadLine();
                switch (command)
                {
                    case "1":
                        Console.Write("Введите длину ключа: ");
                        keyLength = int.Parse(Console.ReadLine());
                        CreateKeyFile(keyLength, keyFile);
                        break;

                    case "2":
                        EncryptText(textFile, ciphertextFile, keyFile);
                        break;

                    case "3":
                        Console.Write("Введите длину ключа: ");
                        keyLength = int.Parse(Console.ReadLine());
                        break;

                    case "4":
                        ForbiddenBigramsSet(sampleTextFile, alphabetFile, forbiddenBigramsFile);
                        break;

                    case "5":
                        GetTable(keyLength, ciphertextFile, forbiddenBigramsFile, tableFile);
                        break;

                    case "6":
                        GetForest(tableFile, forestFile);
                        break;

                    case "7":
                        TestKeys(forestFile, ciphertextFile);
                        break;

                    case "exit":
                        return;

                    default:
                        Console.WriteLine("Введена неверная команда.");
                        break;
                }
            }
        }

        static void ForbiddenBigramsSet(string textFile, string alphabetFile, string forbiddenBigramsFile)
        {
            string text = Regex.Replace(File.ReadAllText(textFile), @"[\p{P}\r\n473½àâè$1820695é	£]", "").ToLower();
            string alphabet = "";
            foreach (char c in text)
            {
                if (!alphabet.Contains(c))
                {
                    alphabet += c;
                }
            }
            File.WriteAllText(alphabetFile, alphabet);

            string forbiddenBigrams = "";
            foreach (char i in alphabet)
            {
                foreach (char j in alphabet)
                {
                    string testBigram = i + "" + j;
                    if (!text.Contains(testBigram))
                    {
                        forbiddenBigrams += testBigram + "\n";
                    }
                }
            }

            File.WriteAllText(forbiddenBigramsFile, forbiddenBigrams.Substring(0, forbiddenBigrams.Length - 1));
        }

        static void GetTable(int keyLength, string ciphertextFile, string forbiddenBigramsFile, string tableFile)
        {
            string[] forbiddenBigrams = File.ReadAllLines(forbiddenBigramsFile);

            string ciphertext = File.ReadAllText(ciphertextFile);

            bool[,] table = new bool[keyLength, keyLength];

            string[] splitCiphertext = new string[ciphertext.Length / keyLength];
            for (int i = 0; i < ciphertext.Length; i += keyLength)
            {
                splitCiphertext[i / keyLength] = ciphertext.Substring(i, keyLength); 
            }

            for (int i = 0; i < keyLength; i++)
            {
                for (int j = 0; j < keyLength; j++)
                {
                    if (i == j)
                    {
                        table[i, j] = true;
                    }
                    else
                    {
                        for (int k = 0; k < splitCiphertext.Length; k++)
                        {
                            if (forbiddenBigrams.Contains(splitCiphertext[k][i] + "" + splitCiphertext[k][j]))
                            {
                                table[i, j] = true;
                                break;
                            }
                        }
                    }
                }
            }

            string strOut = " ";
            for (int i = 0; i < keyLength; i++)
            {
                strOut += " " + i;
            }
            strOut += "\n";
            for (int i = 0; i < keyLength; i++)
            {
                strOut += i;

                for (int j = 0; j < keyLength; j++)
                {
                    if (table[i, j])
                    {
                        strOut += " X";
                    }
                    else
                    {
                        strOut += " .";
                    }
                }

                strOut += "\n";
            }

            File.WriteAllText(tableFile, strOut.Substring(0, strOut.Length - 1));
        }

        static List<string> tree = new List<string>();
        static bool[] used;
        static void GetForest(string tableFile, string forestFile)
        {
            string[] lines = File.ReadAllLines(tableFile);
            bool[,] table = new bool[lines.Length - 1, lines.Length - 1];
            for (int i = 1; i < lines.Length; i++)
            {
                string[] line = lines[i].Split(new char[] { ' ' });
                for (int j = 1; j < line.Length; j++)
                {
                    if (line[j] == "X")
                    {
                        table[i - 1, j - 1] = true;
                    }
                    else
                    {
                        table[i - 1, j - 1] = false;
                    }
                }
            }

            List<int> startVertex = new List<int>();
            int count = 0, min = table.GetLength(0);
            for (int i = 0; i < table.GetLength(0); i++)
            {
                count = 0;
                for (int j = 0; j < table.GetLength(1); j++)
                {
                    if (!table[j, i])
                        count++;
                }
                if (count < min)
                    min = count;
            }
            for (int i = 0; i < table.GetLength(0); i++)
            {
                count = 0;
                for (int j = 0; j < table.GetLength(1); j++)
                {
                    if (!table[j, i])
                        count++;
                }
                if (count == min)
                    startVertex.Add(i);
            }
            List<string>[] forest = new List<string>[startVertex.Capacity];
            
            for (int i = 0; i < startVertex.Count; i++)
            {
                tree.Clear();
                used = new bool[table.GetLength(0)];
                BuildTree(startVertex[i], startVertex[i].ToString(), 0, table);
                List<String> copy = new List<String>(tree);
                forest[i] = copy;
            }

            string strOut = "";
            for (int i = 0; i < forest.Length; i++)
            {
                if (forest[i] == null)
                {
                    continue;
                }

                strOut += "---\n";
                for (int j = 0; j < forest[i].Count; j++)
                {
                    strOut += forest[i][j] + "\n";
                }
            }
            File.WriteAllText(forestFile, strOut);
        }

        static void BuildTree(int v, string path, int depth, bool[,] table)
        {
            depth++;
            if (depth > 1)
                path += " " + v;
            if (depth == table.GetLength(0))
            {
                tree.Add(path);
                return;
            }
            int counter = 0;
            for (int i = 0; i < table.GetLength(0); i++)
            {
                if (!table[v, i] && !path.Contains(i.ToString()))
                {
                    BuildTree(i, path, depth, table);
                    counter++;
                }
            }
            if (counter == 0)
                tree.Add(path);
        }

        static void TestKeys(string forestFile, string ciphertextFile)
        {
            string[] lines = File.ReadAllLines(forestFile);

            for (int i = 0; i < lines.Length; i++)
            {
                if (lines[i].Length >= keyLength * 2 - 1)
                {
                    int[] key = lines[i]
                        .Split(new char[] { ' ', ',', '\n' }, StringSplitOptions.RemoveEmptyEntries)
                        .Select(n => int.Parse(n))
                        .ToArray();

                    DecryptText(ciphertextFile, @"..\..\files\" + lines[i] + ".txt", key);
                }
            }
        }



        static void CreateKeyFile(int keyLength, string keyFile)
        {
            Console.WriteLine("Генерация ключа длины " + keyLength);
            SimplePermutation encryptor = new SimplePermutation();
            encryptor.GenerateKey(keyLength);

            int[] key = encryptor.GetKey();

            StringBuilder keyString = new StringBuilder();
            foreach (int k in key)
            {
                keyString.Append(k + " ");
            }

            File.WriteAllText(keyFile, keyString.ToString());
            Console.WriteLine("Сгенерирован ключ: " + keyString.ToString() + "\n");
        }

        static void EncryptText(string textFile, string ciphertextFile, string keyFile)
        {
            int[] key = File.ReadAllLines(keyFile)[0]
                    .Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(n => int.Parse(n))
                    .ToArray();

            SimplePermutation encryptor = new SimplePermutation(key);

            string text = Regex.Replace(File.ReadAllText(textFile), @"[\p{P}\r\n]", "").ToLower();
            string ciphertext = encryptor.Encrypt(text);

            File.WriteAllText(ciphertextFile, ciphertext);
            Console.WriteLine("Шифрование текста завершено.\n");
        }

        static void DecryptText(string ciphertextFile, string decryptedTextFile, string keyFile)
        {
            int[] key = File.ReadAllLines(keyFile)[0]
                    .Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(n => int.Parse(n))
                    .ToArray();

            SimplePermutation decryptor = new SimplePermutation(key);

            string ciphertext = File.ReadAllText(ciphertextFile);
            string decryptedText = decryptor.Decrypt(ciphertext);

            File.WriteAllText(decryptedTextFile, decryptedText);
            Console.WriteLine("Дешифрование текста завершено.\n");
        }

        static void DecryptText(string ciphertextFile, string decryptedTextFile, int[] key)
        {
            SimplePermutation decryptor = new SimplePermutation(key);

            string ciphertext = File.ReadAllText(ciphertextFile);
            string decryptedText = decryptor.Decrypt(ciphertext);

            File.WriteAllText(decryptedTextFile, decryptedText);
        }
    }
}
