﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3
{
    class SimplePermutation
    {
        private int blockLength;
        private int[] key;

        public SimplePermutation() { }

        public SimplePermutation(int[] key)
        {
            SetKey(key);
        }

        public int[] GetKey()
        {
            return (int[])key.Clone();
        }

        public void SetKey(int[] key)
        {
            this.key = (int[])key.Clone();
            blockLength = key.Length;
        }

        public void GenerateKey(int blockLength)
        {
            this.blockLength = blockLength;
            int[] iterator = new int[blockLength];

            for (int i = 0; i < blockLength - 1; i++)
            {
                iterator[i] = i + 1;
            }

            Random random = new Random();
            for (int i = 0; i < blockLength - 1; i++)
            {
                Swap(iterator, i, random.Next(blockLength - 1));
            }
            iterator[blockLength - 1] = 0;
            key = new int[blockLength];
            key[0] = iterator[0];
            for (int i = 1; i < blockLength; i++)
            {
                key[iterator[i - 1]] = iterator[i];
            }
        }

        private void Swap(int[] arr, int i, int j)
        {
            int temp = arr[i];
            arr[i] = arr[j];
            arr[j] = temp;
        }

        public string Encrypt(string text)
        {
            while (text.Length % blockLength != 0)
            {
                text += " ";
            }

            int[] invKey = new int[blockLength];

            for (int i = 0; i < blockLength; i++)
            {
                invKey[key[i]] = i;
            }

            StringBuilder ciphertext = new StringBuilder();
            for (int i = 0; i < text.Length; i++)
            {
                ciphertext.Append(text[((i / blockLength) * blockLength) + invKey[i % blockLength]]);
            }

            return ciphertext.ToString( );
        }

        public string Decrypt(string ciphertext)
        {
            while (ciphertext.Length % blockLength != 0)
            {
                ciphertext += " ";
            }

            StringBuilder text = new StringBuilder();
            for (int i = 0; i < ciphertext.Length; i++)
            {
                text.Append(ciphertext[((i / blockLength) * blockLength) + key[i % blockLength]]);
            }

            return text.ToString();
        }
    }
}
