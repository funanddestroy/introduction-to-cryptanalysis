﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;

namespace _11
{
    static class Program
    {
        static string nFile = @"..\..\files\n.txt";
        static string kFile = @"..\..\files\k.txt";
        static string lFile = @"..\..\files\l.txt";

        static void Main(string[] args)
        {
            BigInteger n = BigInteger.Parse(File.ReadAllLines(nFile)[0]);
            int k = int.Parse(File.ReadAllLines(kFile)[0]);
            int l = int.Parse(File.ReadAllLines(lFile)[0]);
            
            int i = 1;
            while (true)
            {
                if (i % l == 0)
                {
                    Console.WriteLine("Прошло " + i + " вычислений. Осуществить следующие " + l + " вычислений: Y/N");
                    ConsoleKeyInfo command = Console.ReadKey();
                    Console.WriteLine();
                    if (command.Key == ConsoleKey.N)
                    {
                        break;
                    }
                }

                BigInteger s = Sqrt(k * n) + i;
                i++;
                BigInteger skn = s * s - k * n;
                BigInteger t = Sqrt(skn);
                if (t * t == skn)
                {
                    Console.WriteLine("p = " + BigInteger.GreatestCommonDivisor(k * n, s - t));
                }
            }
        }

        static BigInteger Sqrt(this BigInteger n)
        {
            if (n == 0) return 0;
            if (n > 0)
            {
                int bitLength = Convert.ToInt32(Math.Ceiling(BigInteger.Log(n, 2)));
                BigInteger root = BigInteger.One << (bitLength / 2);

                while (!isSqrt(n, root))
                {
                    root += n / root;
                    root /= 2;
                }

                return root;
            }

            throw new ArithmeticException("NaN");
        }

        static bool isSqrt(BigInteger n, BigInteger root)
        {
            BigInteger lowerBound = root * root;
            BigInteger upperBound = (root + 1) * (root + 1);

            return (n >= lowerBound && n < upperBound);
        }
    }
}
