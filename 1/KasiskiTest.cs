﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1
{
    class KasiskiTest
    {
        private string ciphertext;

        public KasiskiTest(string ciphertext)
        {
            this.ciphertext = ciphertext;
        }

        public IEnumerable<KeyValuePair<int, int>> GetKeyLength() 
        {
            List<int> distances = GetDistances(3, 9);
            return GetMostFrequentGCD(distances);
        }

        private List<int> GetDistances(int minBlock, int maxBlock)
        {
            string block;
            int pos, dist;
            List<int> distances = new List<int>();
            HashSet<string> checkedBlocks = new HashSet<string>();

            for (int blockLength = minBlock; blockLength < Math.Min(maxBlock + 1, ciphertext.Length); blockLength++)
            {
                for (int j = 0; j < ciphertext.Length - 2 * blockLength + 1; j++)
                {
                    block = ciphertext.Substring(j, blockLength);

                    if (!checkedBlocks.Contains(block))
                    {
                        checkedBlocks.Add(block);
                        pos = j;
                        do
                        {
                            dist = pos;
                            pos = ciphertext.IndexOf(block, pos + blockLength);
                            dist = pos - dist;
                            if (dist > 0)
                                distances.Add(dist);
                        }
                        while (pos != -1);
                    }
                }
            }
            return distances;
        }

        private IEnumerable<KeyValuePair<int, int>> GetMostFrequentGCD(List<int> distances)
        {
            Dictionary<int, int> frequencies = new Dictionary<int, int>();
            int g;
            for (int i = 0; i < distances.Count; i++)
            {
                for (int j = i; j < distances.Count; j++)
                {
                    g = GCD(distances[i], distances[j]);
                    if (g < 3)
                        continue;
                    if (frequencies.ContainsKey(g))
                        frequencies[g]++;
                    else
                        frequencies.Add(g, 1);
                }
            }

            return frequencies.OrderByDescending(p => p.Value).Take(5);
        }

        private int GCD(int a, int b)
        {
            if (b == 0)
            {
                return a;
            }
            else
            {
                return GCD(b, a % b);
            }
        }
    }
}
