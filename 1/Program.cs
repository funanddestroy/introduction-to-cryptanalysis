﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1
{
    class Program
    {
        static void Main(string[] args)
        {
            string keyFile = @"..\..\files\key.txt";
            string textFile = @"..\..\files\text.txt";
            string ciphertextFile = @"..\..\files\ciphertext.txt";
            string decryptedTextFile = @"..\..\files\decrypted_text.txt";
            string bruteforceFile = @"..\..\files\bruteforce.txt";
            string kasiskiTestFile = @"..\..\files\kasiski_test.txt";

            Console.WriteLine("1 - Генерация ключа;\n2 - Шифрование текста;\n3 - Дешифрование текста;\n4 - Тест Касиски;\n5 - Перебор ключей заданной длины.\n");

            while (true)
            {
                string command = Console.ReadLine();
                switch (command)
                { 
                    case "1":
                        Console.Write("Введите длину ключа: ");
                        int keyLength = int.Parse(Console.ReadLine());
                        CreateKeyFile(keyLength, keyFile);
                        break;

                    case "2":
                        EncryptText(textFile, ciphertextFile, keyFile);
                        break;

                    case "3":
                        DecryptText(ciphertextFile, decryptedTextFile, keyFile);
                        break;

                    case "4":
                        GetLengthKey(ciphertextFile, kasiskiTestFile);
                        break;

                    case "5":
                        Console.Write("Введите длину ключа для перебора: ");
                        keyLength = int.Parse(Console.ReadLine());
                        Bruteforce(keyLength, ciphertextFile, bruteforceFile);
                        break;

                    case "exit":
                        return;

                    default:
                        Console.WriteLine("Введена неверная команда.");
                        break;
                }
            }
        }

        static void CreateKeyFile(int keyLength, string keyFile)
        {
            Console.WriteLine("Генерация ключа длины " + keyLength);
            SimplePermutation encryptor = new SimplePermutation();
            encryptor.GenerateKey(keyLength);

            int[] key = encryptor.GetKey();

            StringBuilder keyString = new StringBuilder();
            foreach (int k in key)
            {
                keyString.Append(k + " ");
            }

            File.WriteAllText(keyFile, keyString.ToString());
            Console.WriteLine("Сгенерирован ключ: " + keyString.ToString() + "\n");
        }

        static void EncryptText(string textFile, string ciphertextFile, string keyFile)
        {
            int[] key = File.ReadAllLines(keyFile)[0]
                    .Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(n => int.Parse(n))
                    .ToArray();

            SimplePermutation encryptor = new SimplePermutation(key);

            string text = File.ReadAllText(textFile);
            string ciphertext = encryptor.Encrypt(text);

            File.WriteAllText(ciphertextFile, ciphertext);
            Console.WriteLine("Шифрование текста завершено.\n");
        }

        static void DecryptText(string ciphertextFile, string decryptedTextFile, string keyFile)
        {
            int[] key = File.ReadAllLines(keyFile)[0]
                    .Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(n => int.Parse(n))
                    .ToArray();

            SimplePermutation decryptor = new SimplePermutation(key);

            string ciphertext = File.ReadAllText(ciphertextFile);
            string decryptedText = decryptor.Decrypt(ciphertext);

            File.WriteAllText(decryptedTextFile, decryptedText);
            Console.WriteLine("Дешифрование текста завершено.\n");
        }

        static void GetLengthKey(string ciphertextFile, string kasiskiTestFile)
        {
            string ciphertext = File.ReadAllText(ciphertextFile);
            KasiskiTest test = new KasiskiTest(ciphertext);
            var keyLength = test.GetKeyLength();

            StringBuilder tOut = new StringBuilder();
            tOut.Append(string.Format("{0,10}{1, 15}\r\n", "Длина", "Повторения"));
            Console.WriteLine("{0,10}{1,15}", "Длина", "Повторения");

            foreach (var pair in keyLength)
            {
                tOut.Append(string.Format("{0,10}{1,15}\r\n", pair.Key, pair.Value));
                Console.WriteLine("{0,10}{1,15}", pair.Key, pair.Value);
            }
            Console.WriteLine();

            File.WriteAllText(kasiskiTestFile, tOut.ToString());
        }

        static void Bruteforce(int keyLength, string ciphertextFile, string bruteforceFile)
        {
            BruteforceKey bk = new BruteforceKey(keyLength);
            bk.Generate();
            List<int[]> keys = bk.GetListPermutations();

            StreamWriter writer = new StreamWriter(new FileStream(bruteforceFile, FileMode.Create));

            foreach (int[] key in keys)
            {
                string strKey = "";
                for (int i = 0; i < key.Length; i++)
                {
                    key[i]--;
                    strKey += key[i] + " ";
                }

                SimplePermutation decryptor = new SimplePermutation(key);

                string ciphertext = File.ReadAllText(ciphertextFile);
                string decryptedText = decryptor.Decrypt(ciphertext);
                
                writer.WriteLine("Перестановка: " + strKey);
                writer.WriteLine(decryptedText);
                writer.WriteLine();
            }

            writer.Close();

            Console.WriteLine("Перебор ключей длины " + keyLength + " завершен.\n");
        }
    }
}
