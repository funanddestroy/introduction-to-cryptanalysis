﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1
{
    class BruteforceKey
    {
        private int n;
        private int[] arr;
        private List<int[]> ans;

        public BruteforceKey(int n)
        {
            this.n = n;

            arr = new int[n];
            for (int i = 0; i < n; i++)
            {
                arr[i] = i + 1;
            }

            ans = new List<int[]>();
        }

        public List<int[]> GetListPermutations()
        {
            return new List<int[]>(ans.Select(x => (int[])x.Clone()));//ans;
        }

        private void Swap(int[] arr, int i, int j) //поменять местами i-ый и j-ый элемент
        {
            int temp = arr[i];
            arr[i] = arr[j];
            arr[j] = temp;
        }

        private bool IsCorrect(int[] arr) //проверка на моноцикличность
        {
            int first = arr[0];
            int temp = first;
            int i = 0;
            while (i < arr.Length && first != arr[temp - 1])
            {
                i++;
                temp = arr[temp - 1];
            }
            return i == arr.Length - 1 && first == arr[temp - 1];
        }

        public void Generate(int k = 0) //генерация всех перестановок длины n
        {
            if (k == n)
            {
                if (IsCorrect(arr))
                {
                    ans.Add((int[])arr.Clone());
                }
            }
            else
            {
                for (int j = k; j < arr.Length; j++)
                {
                    Swap(arr, k, j);
                    Generate(k + 1);
                    Swap(arr, k, j);
                }
            }
        }
    }
}
