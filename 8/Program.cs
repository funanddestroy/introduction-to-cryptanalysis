﻿using System;
using System.IO;
using System.Numerics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _8
{
    class Program
    {
        static string primeFile = @"..\..\files\p.txt";
        static string tFile = @"..\..\files\t.txt";
        static string tPrimeFile = @"..\..\files\tp.txt";
        static string nFile = @"..\..\files\n.txt";
        static string factorFile = @"..\..\files\factor.txt";

        static void Main(string[] args)
        {
            Console.WriteLine("1 - Создание базы данных из произведений простых чисел;\n2 - Метод пробного деления.\n");

            while (true)
            {
                string command = Console.ReadLine();
                switch (command)
                {
                    case "1":
                        GetCompositions(primeFile, tFile, tPrimeFile);
                        break;

                    case "2":
                        Factorisation(nFile, tPrimeFile, factorFile, primeFile);
                        break;

                    case "exit":
                        return;

                    default:
                        Console.WriteLine("Введена неверная команда.");
                        break;
                }
            }
        }

        static void GetCompositions(string primeFile, string tFile, string tPrimeFile)
        {
            string[] lines = File.ReadAllLines(primeFile);
            BigInteger[] prime = new BigInteger[lines.Length];
            for (int i = 0; i < lines.Length; i++)
            {
                prime[i] = BigInteger.Parse(lines[i]);
            }

            int t = int.Parse(File.ReadAllText(tFile));

            string sOut = "";
            for (int i = 0; i < prime.Length - t; i += t)
            {
                BigInteger p = 1;
                for (int j = i; j < i + t; j++)
                {
                    p *= prime[j];
                }
                sOut += p + "\n";
            }

            File.WriteAllText(tPrimeFile, sOut.Substring(0, sOut.Length - 1));
            Console.WriteLine("Базы данных из произведений простых чисел создана");
        }

        static void Factorisation(string nFile, string tPrimeFile, string factorFile, string primeFile)
        {
            BigInteger n = BigInteger.Parse(File.ReadAllText(nFile));
            BigInteger nn = n;

            string[] lines = File.ReadAllLines(tPrimeFile);
            BigInteger[] tp = new BigInteger[lines.Length];
            for (int i = 0; i < lines.Length; i++)
            {
                tp[i] = BigInteger.Parse(lines[i]);
            }

            lines = File.ReadAllLines(primeFile);
            BigInteger[] prime = new BigInteger[lines.Length];
            for (int i = 0; i < lines.Length; i++)
            {
                prime[i] = BigInteger.Parse(lines[i]);
            }

            Dictionary<BigInteger, int> factor = new Dictionary<BigInteger, int>();
            
            for (int i = 0; i < tp.Length; i++)
            {
                BigInteger d = BigInteger.GreatestCommonDivisor(n, tp[i]);
                while (d != 1)
                {
                    n = n / d;
                    int j = 0;
                    while (prime[j] <= d)
                    {
                        if (d % prime[j] == 0)
                        {
                            if (!factor.Keys.Contains(d))
                            {
                                factor.Add(prime[j], 1);
                            }
                            else
                            {
                                factor[prime[j]]++;
                            }
                        }
                        j++;
                    }                    

                    d = BigInteger.GreatestCommonDivisor(n, d);
                }
            }

            BigInteger number = 1;
            string sOut = "";
            foreach (var f in factor)
            {
                sOut += f.Key;
                if (f.Value > 1)
                {
                    sOut += "^" + f.Value;
                    number *= BigInteger.Pow(f.Key, f.Value);
                }
                else
                {
                    number *= f.Key;
                }
                sOut += " * ";
            }

            if ((nn / number) != 1)
            {
                File.WriteAllText(factorFile, sOut.Substring(0, sOut.Length - 3) + " * " + (nn / number) + " = " + nn);
                Console.WriteLine("Разложение числа получено:\n" + sOut.Substring(0, sOut.Length - 3) + " * " + (nn / number) + " = " + nn);
            }
            else
            {
                File.WriteAllText(factorFile, sOut.Substring(0, sOut.Length - 3) + " = " + nn);
                Console.WriteLine("Разложение числа получено:\n" + sOut.Substring(0, sOut.Length - 3) + " = " + nn);
            }
            
        }
    }
}
