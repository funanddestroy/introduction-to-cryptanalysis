﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using _2;
using System.Text;
using System.Threading.Tasks;

namespace _6
{
    class Program
    {
        static string textFile = @"..\..\files\text.txt";
        static string sampleTextFile = @"..\..\files\sample_text.txt";
        static string alphabetFile = @"..\..\files\alphabet.txt";
        static string alphabetFrequencyFile = @"..\..\files\alphabet_frequency.txt";
        static string keyFile = @"..\..\files\key.txt";
        static string ciphertextFile = @"..\..\files\ciphertext.txt";
        static string decryptedTextFile = @"..\..\files\decrypted_text.txt";
        static string keyLengthFile = @"..\..\files\key_length.txt";
        static string testKeysFile = @"..\..\files\test_keys.txt";

        static void Main(string[] args)
        {
            Console.WriteLine("1 - Шифрование текста;\n2 - Метод Симпсона.\n");

            while (true)
            {
                string command = Console.ReadLine();
                switch (command)
                {
                    case "1":
                        EncryptText(textFile, ciphertextFile, keyFile, alphabetFile);
                        break;

                    case "2":
                        SimpsonsMethod(ciphertextFile, alphabetFrequencyFile, keyLengthFile, alphabetFile, testKeysFile);
                        break;

                    case "exit":
                        return;

                    default:
                        Console.WriteLine("Введена неверная команда.");
                        break;
                }
            }
        }

        static void SimpsonsMethod(string ciphertextFile, string alphabetFrequencyFile, string keyLengthFile, string alphabetFile, string testKeysFile)
        {
            int testLength = 3;
            string ciphertext = File.ReadAllText(ciphertextFile);
            string alphabet = File.ReadAllText(alphabetFile);
            int keyLength = int.Parse(File.ReadAllText(keyLengthFile));

            Dictionary<char, double> alphabetFrequency = new Dictionary<char, double>();
            string[] lines = File.ReadAllLines(alphabetFrequencyFile);
            foreach (string line in lines)
            {
                string[] str = line.Split(new char[] { '|' });
                alphabetFrequency.Add(str[0][0], double.Parse(str[1]));
            }

            char[] testLetters = new char[testLength];
            int count = 0;
            foreach (var pair in alphabetFrequency)
            {
                if (count >= testLength)
                {
                    break;
                }
                testLetters[count++] = pair.Key;
            }

            List<char> k1 = new List<char>();
            string Y0 = "";
            for (int j = 0; j < ciphertext.Length; j += keyLength)
            {
                Y0 += ciphertext[j];
            }

            Dictionary<char, double> partFrequency = GetLetterFrequency(Y0);

            count = 0;
            foreach (var pair in partFrequency)
            {
                if (count >= testLength)
                {
                    break;
                }

                char c = alphabet[((alphabet.IndexOf(pair.Key) - alphabet.IndexOf(testLetters[count++])) % alphabet.Length + alphabet.Length) % alphabet.Length];

                if (!k1.Contains(c))
                {
                    k1.Add(c);
                }
            }

            char[] delta = new char[keyLength];
            for (int i = 1; i < keyLength; i++)
            {
                string part = "";
                for (int j = i; j < ciphertext.Length; j += keyLength)
                {
                    part += ciphertext[j];
                }

                double II = 0;
                for (int j = 1; j < alphabet.Length; j++)
                {
                    string Yj = "";
                    for (int h = 0; h < part.Length; h++)
                    {
                        Yj += alphabet[((alphabet.IndexOf(part[h]) - j) % alphabet.Length + alphabet.Length) % alphabet.Length];
                    }

                    double I = Index.AverageCoincidenceIndex(Y0, Yj);

                    if (I > II)
                    {
                        II = I;
                        delta[i] = alphabet[j];
                    }
                }
            }

            List<string> keys = new List<string>();

            Console.WriteLine("Возможные ключи:");
            for (int i = 0; i < k1.Count; i++)
            {
                string K = k1[i] + "";
                for (int j = 1; j < keyLength; j++)
                {
                    K += alphabet[((alphabet.IndexOf(k1[i]) + alphabet.IndexOf(delta[j])) % alphabet.Length + alphabet.Length) % alphabet.Length];
                }
                Console.WriteLine(K);
                keys.Add(K);
            }

            string strOut = "";
            foreach (string key in keys)
            {
                strOut += key + "\n";
                strOut += DecryptText(ciphertextFile, key, alphabetFile);
                strOut += "\n\n";
            }

            File.WriteAllText(testKeysFile, strOut);

        }


        static void GetLetterFrequency(string textFile, string alphabetFrequencyFile)
        {
            string text = Regex.Replace(File.ReadAllText(textFile), @"[\p{P}\r\n473½àâè$1820695é	£]", "").ToLower();

            Dictionary<char, double> alphabet = new Dictionary<char, double>();
            for (int i = 0; i < text.Length; i++)
            {
                if (!alphabet.Keys.Contains(text[i]))
                {
                    double amount = new Regex(text[i].ToString()).Matches(text).Count;
                    alphabet.Add(text[i], amount / text.Length);
                }
            }

            alphabet = alphabet.OrderByDescending(pair => pair.Value).ToDictionary(pair => pair.Key, pair => pair.Value);
            string strOut = "";
            foreach (var item in alphabet)
            {
                strOut += item.Key + "|" + item.Value + "\n";
            }

            File.WriteAllText(alphabetFrequencyFile, strOut.Substring(0, strOut.Length - 1));
        }

        static Dictionary<char, double> GetLetterFrequency(string text)
        {
            Dictionary<char, double> alphabet = new Dictionary<char, double>();
            for (int i = 0; i < text.Length; i++)
            {
                if (!alphabet.Keys.Contains(text[i]))
                {
                    double amount = new Regex(text[i].ToString()).Matches(text).Count;
                    alphabet.Add(text[i], amount / text.Length);
                }
            }

            alphabet = alphabet.OrderByDescending(pair => pair.Value).ToDictionary(pair => pair.Key, pair => pair.Value);

            return alphabet;
        }

        static void EncryptText(string textFile, string ciphertextFile, string keyFile, string alphabetFile)
        {
            string text = Regex.Replace(File.ReadAllText(textFile), @"[\p{P}\r\n]", "").ToLower();
            string ciphertext = new VigenereCipher(File.ReadAllText(keyFile), File.ReadAllText(alphabetFile)).Encrypt(text);

            File.WriteAllText(ciphertextFile, ciphertext);
            Console.WriteLine("Шифрование текста завершено.\n");
        }

        static void DecryptText(string ciphertextFile, string decryptedTextFile, string keyFile, string alphabetFile)
        {
            string ciphertext = File.ReadAllText(ciphertextFile);
            string decryptedText = new VigenereCipher(File.ReadAllText(keyFile), File.ReadAllText(alphabetFile)).Decrypt(ciphertext);

            File.WriteAllText(decryptedTextFile, decryptedText);
            Console.WriteLine("Дешифрование текста завершено.\n");
        }

        static string DecryptText(string ciphertextFile, string key, string alphabetFile)
        {
            string ciphertext = File.ReadAllText(ciphertextFile);
            string decryptedText = new VigenereCipher(key, File.ReadAllText(alphabetFile)).Decrypt(ciphertext);

            return decryptedText;
        }
    }
}
