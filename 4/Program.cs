﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using _2;
using System.Text;
using System.Threading.Tasks;

namespace _4
{
    class Program
    {
        static string textFile = @"..\..\files\text.txt";
        static string sampleTextFile = @"..\..\files\sample_text.txt";
        static string alphabetFile = @"..\..\files\alphabet.txt";
        static string alphabetFrequencyFile = @"..\..\files\alphabet_frequency.txt";
        static string keyFile = @"..\..\files\key.txt";
        static string ciphertextFile = @"..\..\files\ciphertext.txt";
        static string decryptedTextFile = @"..\..\files\decrypted_text.txt";
        static string testKeysFile = @"..\..\files\test_keys.txt";
        static string wordFrequencyFile = @"..\..\files\word_frequency.txt";
        static string testKeysWordFile = @"..\..\files\test_keys_word.txt";


        static int keyLength = 5;

        static void Main(string[] args)
        {
            Console.WriteLine("1 - Шифрование текста;\n2 - Ввод длины ключа;\n3 - Вычисление частот символов текста;\n4 - Метод чтения по колонкам;\n5 - Вычисление частот слов текста;\n6 - Метод протяжки вероятного слова.\n");

            while (true)
            {
                string command = Console.ReadLine();
                switch (command)
                {
                    case "1":
                        EncryptText(textFile, ciphertextFile, keyFile, alphabetFile);
                        break;

                    case "2":
                        Console.Write("Введите длину ключа: ");
                        keyLength = int.Parse(Console.ReadLine());
                        break;

                    case "3":
                        GetLetterFrequency(sampleTextFile, alphabetFrequencyFile);
                        break;

                    case "4":
                        TestKeys(keyLength, ciphertextFile, alphabetFrequencyFile, alphabetFile, testKeysFile);
                        break;

                    case "5":
                        GetWordFrequency(keyLength, sampleTextFile, wordFrequencyFile);
                        break;

                    case "6":
                        TestWords(keyLength, ciphertextFile, wordFrequencyFile, alphabetFile, testKeysWordFile);
                        break;

                    case "exit":
                        return;

                    default:
                        Console.WriteLine("Введена неверная команда.");
                        break;
                }
            }
        }

        static void GetLetterFrequency(string textFile, string alphabetFrequencyFile)
        {
            string text = Regex.Replace(File.ReadAllText(textFile), @"[\p{P}\r\n473½àâè$1820695é	£]", "").ToLower();

            Dictionary<char, double> alphabet = new Dictionary<char, double>();
            for (int i = 0; i < text.Length; i++)
            {
                if (!alphabet.Keys.Contains(text[i]))
                {
                    double amount = new Regex(text[i].ToString()).Matches(text).Count;
                    alphabet.Add(text[i], amount / text.Length);
                }
            }

            alphabet = alphabet.OrderByDescending(pair => pair.Value).ToDictionary(pair => pair.Key, pair => pair.Value);
            string strOut = "";
            foreach (var item in alphabet)
            {
                strOut += item.Key + "|" + item.Value + "\n";
            }

            File.WriteAllText(alphabetFrequencyFile, strOut.Substring(0, strOut.Length - 1));
        }

        static Dictionary<char, double> GetLetterFrequency(string text)
        {
            Dictionary<char, double> alphabet = new Dictionary<char, double>();
            for (int i = 0; i < text.Length; i++)
            {
                if (!alphabet.Keys.Contains(text[i]))
                {
                    double amount = new Regex(text[i].ToString()).Matches(text).Count;
                    alphabet.Add(text[i], amount / text.Length);
                }
            }

            alphabet = alphabet.OrderByDescending(pair => pair.Value).ToDictionary(pair => pair.Key, pair => pair.Value);

            return alphabet;
        }

        static void TestKeys(int keyLength, string ciphertextFile, string alphabetFrequencyFile, string alphabetFile, string testKeysFile)
        {
            int testLength = 3;
            string ciphertext = File.ReadAllText(ciphertextFile);
            string alphabet = File.ReadAllText(alphabetFile);

            Dictionary<char, double> alphabetFrequency = new Dictionary<char, double>();
            string[] lines = File.ReadAllLines(alphabetFrequencyFile);
            foreach (string line in lines)
            {
                string[] str = line.Split(new char[] { '|' });
                alphabetFrequency.Add(str[0][0], double.Parse(str[1]));
            }

            char[] testLetters = new char[testLength];
            int count = 0;
            foreach (var pair in alphabetFrequency)
            {
                if (count >= testLength)
                {
                    break;
                }
                testLetters[count++] = pair.Key;
            }

            List<char>[] lettersSet = new List<char>[keyLength];
            for (int i = 0; i < keyLength; i++)
            {
                lettersSet[i] = new List<char>(); 
                string part = "";
                for (int j = i; j < ciphertext.Length; j += keyLength)
                {
                    part += ciphertext[j];
                }

                Dictionary<char, double> partFrequency = GetLetterFrequency(part);

                count = 0;
                foreach (var pair in partFrequency)
                {
                    if (count >= testLength)
                    {
                        break;
                    }

                    char c = alphabet[((alphabet.IndexOf(pair.Key) - alphabet.IndexOf(testLetters[count++])) % alphabet.Length + alphabet.Length) % alphabet.Length];

                    if (!lettersSet[i].Contains(c))
                    {
                        lettersSet[i].Add(c);
                    }
                }
            }

            List<string> keys = new List<string>();
            GetKeys(0, "", lettersSet, ref keys);

            string strOut = "";
            foreach (string key in keys)
            {
                strOut += key + "\n";
                strOut += DecryptText(ciphertextFile, key, alphabetFile);
                strOut += "\n\n";
            }

            File.WriteAllText(testKeysFile, strOut);
        }

        static void GetKeys(int v, string key, List<char>[] lettersSet, ref List<string> keys)
        {
            if (v >= lettersSet.Length)
            {
                keys.Add(key);
                return;
            }

            foreach (char c in lettersSet[v])
            {
                key += c;
                GetKeys(v + 1, key, lettersSet, ref keys);
                key = key.Remove(key.Length - 1, 1);
            }
        }



        static void GetWordFrequency(int wordLength, string textFile, string wordFrequencyFile)
        {
            string text = Regex.Replace(File.ReadAllText(textFile), @"[\p{P}\r\n473½àâè$1820695é	£]", "").ToLower();

            string[] words = text.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

            Dictionary<string, double> wordFrequency = new Dictionary<string, double>();
            for (int i = 0; i < words.Length; i++)
            {
                if (!wordFrequency.Keys.Contains(words[i]) && words[i].Length == wordLength)
                {
                    double amount = new Regex(words[i]).Matches(text).Count;
                    wordFrequency.Add(words[i], amount / words.Length);
                }
            }

            wordFrequency = wordFrequency.OrderByDescending(pair => pair.Value).ToDictionary(pair => pair.Key, pair => pair.Value);
            string strOut = "";
            foreach (var item in wordFrequency)
            {
                strOut += item.Key + "|" + item.Value + "\n";
            }

            File.WriteAllText(wordFrequencyFile, strOut.Substring(0, strOut.Length - 1));
        }

        static void TestWords(int keyLength, string ciphertextFile, string wordFrequencyFile, string alphabetFile, string testKeysWordFile)
        {
            int testLength = 100;
            string ciphertext = File.ReadAllText(ciphertextFile);
            string alphabet = File.ReadAllText(alphabetFile);

            Dictionary<string, double> wordFrequency = new Dictionary<string, double>();
            string[] lines = File.ReadAllLines(wordFrequencyFile);
            foreach (string line in lines)
            {
                string[] str = line.Split(new char[] { '|' });
                wordFrequency.Add(str[0], double.Parse(str[1]));
            }

            string[] testWord = new string[testLength];
            int count = 0;
            foreach (var pair in wordFrequency)
            {
                if (count >= testLength)
                {
                    break;
                }
                testWord[count++] = pair.Key;
            }

            Dictionary<string, int> keyFreq = new Dictionary<string, int>();

            for (int k = 0; k < testLength; k++)
            {
                for (int i = 0; i < ciphertext.Length - keyLength; i ++)
                {
                    string key = "";
                    string word = ciphertext.Substring(i / keyLength, keyLength);

                    string test = testWord[k].Substring(i % keyLength, keyLength - i % keyLength) + testWord[k].Substring(0, i % keyLength);
                    
                    for (int j = 0; j < keyLength; j++)
                    {
                        key += alphabet[((alphabet.IndexOf(word[j]) - alphabet.IndexOf(test[j])) % alphabet.Length + alphabet.Length) % alphabet.Length];
                    }

                    if (!keyFreq.Keys.Contains(key))
                    {
                        keyFreq.Add(key, 0);
                    }
                    else
                    {
                        keyFreq[key]++;
                    }
                }
            }

            keyFreq = keyFreq.OrderByDescending(pair => pair.Value).ToDictionary(pair => pair.Key, pair => pair.Value);

            string strOut = "";
            count = 0;
            foreach (var k in keyFreq)
            {
                if (k.Key.Contains(" ")) { continue; }
                if (count++ >= 100) break;
                strOut += k.Key + "\n";
                strOut += DecryptText(ciphertextFile, k.Key, alphabetFile);
                strOut += "\n\n";
            }
            Console.WriteLine(keyFreq.Keys.Contains("monkey"));

            File.WriteAllText(testKeysWordFile, strOut);
        }



        static void EncryptText(string textFile, string ciphertextFile, string keyFile, string alphabetFile)
        {
            string text = Regex.Replace(File.ReadAllText(textFile), @"[\p{P}\r\n]", "").ToLower();
            string ciphertext = new VigenereCipher(File.ReadAllText(keyFile), File.ReadAllText(alphabetFile)).Encrypt(text); 

            File.WriteAllText(ciphertextFile, ciphertext);
            Console.WriteLine("Шифрование текста завершено.\n");
        }

        static void DecryptText(string ciphertextFile, string decryptedTextFile, string keyFile, string alphabetFile)
        {
            string ciphertext = File.ReadAllText(ciphertextFile);
            string decryptedText = new VigenereCipher(File.ReadAllText(keyFile), File.ReadAllText(alphabetFile)).Decrypt(ciphertext);

            File.WriteAllText(decryptedTextFile, decryptedText);
            Console.WriteLine("Дешифрование текста завершено.\n");
        }

        static string DecryptText(string ciphertextFile, string key, string alphabetFile)
        {
            string ciphertext = File.ReadAllText(ciphertextFile);
            string decryptedText = new VigenereCipher(key, File.ReadAllText(alphabetFile)).Decrypt(ciphertext);

            return decryptedText;
        }
    }
}
