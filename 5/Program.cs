﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using _2;
using System.Text;
using System.Threading.Tasks;

namespace _5
{
    class Program
    {
        static string textFile = @"..\..\files\text.txt";
        static string sampleTextFile = @"..\..\files\sample_text.txt";
        static string alphabetFile = @"..\..\files\alphabet.txt";
        static string keyFile = @"..\..\files\key.txt";
        static string ciphertextFile = @"..\..\files\ciphertext.txt";
        static string decryptedTextFile = @"..\..\files\decrypted_text.txt";
        static string alphabetFrequencyFile = @"..\..\files\alphabet_frequency.txt";
        static string H0File = @"..\..\files\H0.txt";
        static string HdFile = @"..\..\files\Hd.txt";

        static int n1 = 3;
        static int n2 = 10;

        static void Main(string[] args)
        {
            Console.WriteLine("1 - Шифрование текста;\n2 - Вычисление значений гипотезы H(0)\n3 - Вычисление значений гипотезы Н(d) с наиболее вероятной длиной периода d.\n");

            while (true)
            {
                string command = Console.ReadLine();
                switch (command)
                {
                    case "1":
                        EncryptText(textFile, ciphertextFile, keyFile, alphabetFile);
                        break;

                    case "2":
                        GetLetterFrequencyAndH0(sampleTextFile, alphabetFrequencyFile, H0File, alphabetFile);
                        break;

                    case "3":
                        GetHd(ciphertextFile, n1, n2, H0File, alphabetFile, HdFile);
                        break;

                    case "exit":
                        return;

                    default:
                        Console.WriteLine("Введена неверная команда.");
                        break;
                }
            }
        }

        static void GetLetterFrequencyAndH0(string sampleTextFile, string alphabetFrequencyFile, string H0File, string alphabetFile)
        {
            string text = Regex.Replace(File.ReadAllText(sampleTextFile), @"[\p{P}\r\n473½àâè$1820695é	£]", "").ToLower();

            Dictionary<char, double> alphabetFrequency = new Dictionary<char, double>();
            for (int i = 0; i < text.Length; i++)
            {
                if (!alphabetFrequency.Keys.Contains(text[i]))
                {
                    double amount = new Regex(text[i].ToString()).Matches(text).Count;
                    alphabetFrequency.Add(text[i], amount / text.Length);
                }
            }

            alphabetFrequency = alphabetFrequency.OrderByDescending(pair => pair.Value).ToDictionary(pair => pair.Key, pair => pair.Value);
            string strOut = "";
            string strAlph = "";
            foreach (var item in alphabetFrequency)
            {
                strOut += item.Key + "=" + item.Value + "\n";
                strAlph += item.Key;
            }

            File.WriteAllText(alphabetFrequencyFile, strOut.Substring(0, strOut.Length - 1));
            File.WriteAllText(alphabetFile, strAlph);

            List<double> p = new List<double>();
            foreach (var item in alphabetFrequency)
            {
                p.Add(item.Value);
            }            

            int m = alphabetFrequency.Count;
            double[] P = new double[m];

            for (int j = 0; j < m; j++)
            {
                P[j] = 0;
                for (int i = 0; i < m; i++)
                {
                    for (int k = 0; k < m; k++)
                    {
                        if (((i - k) % m + m) % m == j)
                        {
                            P[j] += p[j] * p[k];
                        }
                    }
                }
            }

            strOut = "";
            for (int i = 0; i < P.Length; i++)
            {
                strOut += "P_" + i + "=" + P[i] + "\n"; 
            }

            File.WriteAllText(H0File, strOut.Substring(0, strOut.Length - 1));

            Console.WriteLine("Значения гипотезы H(0) вычисленны.");
        }

        static void GetHd(string ciphertextFile, int n1, int n2, string H0File, string alphabetFile, string HdFile)
        {
            string alphabet = File.ReadAllText(alphabetFile);
            int m = alphabet.Length;

            string ciphertext = File.ReadAllText(ciphertextFile);
            int N = ciphertext.Length;

            string[] lines = File.ReadAllLines(H0File);
            double[] P = new double[lines.Length];
            for (int i = 0; i < lines.Length; i++)
            {
                string[] str = lines[i].Split(new char[] { '=' });
                P[i] = double.Parse(str[1]);
            }

            List<Pair> Hd = new List<Pair>();
            for (int d = n1; d < n2 + 1; d++)
            {
                int t = N / d;
                int r = N % d;
                int[] Z = new int[(t - 1) * d + r];
                for (int i = 0; i < Z.Length; i++)
                {
                    Z[i] = ((alphabet.IndexOf(ciphertext[i]) - alphabet.IndexOf(ciphertext[i + d])) % m + m) % m;
                }

                double[] Pz = new double[m]; 
                for (int i = 0; i < m; i++)
                {
                    Pz[Z[i]]++;
                }
                for (int i = 0; i < m; i++)
                {
                    Pz[i] /= Z.Length;
                }

                double f = 0;
                for (int i = 0; i < m; i++)
                {
                    f += (P[i] - Pz[i]) * (P[i] - Pz[i]);
                }

                Hd.Add(new Pair(f, d));
            }

            Hd.Sort();

            string strOut = "";
            foreach (var item in Hd)
            {
                strOut += item.d + "\n";
            }

            File.WriteAllText(HdFile, strOut.Substring(0, strOut.Length - 1));

            Console.WriteLine("Наиболее вероятные длины периода вычисленны.");
        }



        static void EncryptText(string textFile, string ciphertextFile, string keyFile, string alphabetFile)
        {
            string text = Regex.Replace(File.ReadAllText(textFile), @"[\p{P}\r\n]", "").ToLower();
            string ciphertext = new VigenereCipher(File.ReadAllText(keyFile), File.ReadAllText(alphabetFile)).Encrypt(text);

            File.WriteAllText(ciphertextFile, ciphertext);
            Console.WriteLine("Шифрование текста завершено.");
        }

        static void DecryptText(string ciphertextFile, string decryptedTextFile, string keyFile, string alphabetFile)
        {
            string ciphertext = File.ReadAllText(ciphertextFile);
            string decryptedText = new VigenereCipher(File.ReadAllText(keyFile), File.ReadAllText(alphabetFile)).Decrypt(ciphertext);

            File.WriteAllText(decryptedTextFile, decryptedText);
            Console.WriteLine("Дешифрование текста завершено.");
        }
    }

    class Pair : IComparable
    {
        public double f;
        public int d;

        public Pair(double f, int d)
        {
            this.f = f;
            this.d = d;
        }

        public int CompareTo(object obj)
        {
            Pair o = (Pair)obj;
            return this.f.CompareTo(o.f);
        }
    }
}
