﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using _2;
using System.Text;
using System.Threading.Tasks;

namespace _7
{
    class Program
    {
        static string textFile = @"..\..\files\text.txt";
        static string sampleTextFile = @"..\..\files\sample_text.txt";
        static string alphabetFile = @"..\..\files\alphabet.txt";
        static string bigramTableFile = @"..\..\files\bigram_table.txt";
        static string keyFile = @"..\..\files\key.txt";
        static string ansKeyFile = @"..\..\files\ans_key.txt";
        static string ciphertextFile = @"..\..\files\ciphertext.txt";
        static string decryptedTextFile = @"..\..\files\decrypted_text.txt";
        static string keyLengthFile = @"..\..\files\key_length.txt";

        static void Main(string[] args)
        {
            Console.WriteLine("1 - Шифрование текста;\n2 - Вычисление эталонной матрицы частот биграмм языка открытых сообщений;\n3 - Вычисление ключа.\n");

            while (true)
            {
                string command = Console.ReadLine();
                switch (command)
                {
                    case "1":
                        EncryptText(textFile, ciphertextFile, keyFile, alphabetFile);
                        break;

                    case "2":
                        GetBigramTable(sampleTextFile, alphabetFile, bigramTableFile);
                        break;

                    case "3":
                        GetKey(ciphertextFile, keyLengthFile, bigramTableFile, alphabetFile, ansKeyFile);
                        break;

                    case "exit":
                        return;

                    default:
                        Console.WriteLine("Введена неверная команда.");
                        break;
                }
            }
        }

        static void GetBigramTable(string sampleTextFile, string alphabetFile, string bigramTableFile)
        {
            string sampleText = Regex.Replace(File.ReadAllText(sampleTextFile), @"[\p{P}\r\n473½àâè$1820695é	£]", "").ToLower();

            string alphabet = "";
            foreach (char c in sampleText)
            {
                if (!alphabet.Contains(c))
                {
                    alphabet += c;
                }
            }
            File.WriteAllText(alphabetFile, alphabet);

            string bigramTable = " ";
            foreach (char i in alphabet)
            {
                bigramTable += "|    " + i + "    ";
            }

            foreach (char i in alphabet)
            {
                bigramTable += "\n" + i;
                foreach (char j in alphabet)
                {
                    string testBigram = i + "" + j;
                    double amount = new Regex(testBigram).Matches(sampleText).Count;
                    amount /= sampleText.Length;
                    bigramTable += "|" + String.Format("{0:0.0000000}", amount);
                }
            }

            File.WriteAllText(bigramTableFile, bigramTable);

            Console.WriteLine("Эталонная матрица частот биграмм языка открытых сообщений вычислена");
        }

        static double[,] GetBigramTable(string text, string alphabet)
        {
            double[,] bigramTable = new double[alphabet.Length, alphabet.Length];

            for (int i = 0; i < alphabet.Length; i++)
            {
                for (int j = 0; j < alphabet.Length; j++)
                {
                    string testBigram = alphabet[i] + "" + alphabet[j];
                    double amount = new Regex(testBigram).Matches(text).Count;
                    amount /= text.Length;
                    bigramTable[i, j] = amount;
                }
            }

            return bigramTable;
        }

        static double W(double[,] E, double[,] D)
        {
            double w = 0;
            for (int i = 0; i < E.GetLength(0); i++)
            {
                for (int j = 0; j < E.GetLength(1); j++)
                {
                    w += Math.Abs(E[i, j] - D[i, j]);
                }
            }
            return w;
        }

        static void GetKey(string ciphertextFile, string keyLengthFile, string bigramTableFile, string alphabetFile, string ansKeyFile)
        {
            string[] lines = File.ReadAllLines(bigramTableFile);
            double[,] E = new double[lines.Length - 1, lines.Length - 1];
            for (int i = 1; i < lines.Length; i++)
            {
                string[] line = lines[i].Split(new char[] { '|' });
                for (int j = 1; j < line.Length; j++)
                {
                    E[i - 1, j - 1] = double.Parse(line[j]);
                }
            }

            string ciphertext = File.ReadAllText(ciphertextFile);
            string alphabet = File.ReadAllText(alphabetFile);
            int keyLength = int.Parse(File.ReadAllText(keyLengthFile));

            string k0 = "";
            for (int i = 0; i < keyLength; i++)
            {
                k0 += "b";
            }

            string k = k0;

            do
            {
                k = k0;

                for (int i = 0; i < keyLength; i++)
                {
                    double sw = double.MaxValue;
                    for (int j = 0; j < alphabet.Length; j++)
                    {
                        string testK = k.Remove(i, 1).Insert(i, alphabet[j].ToString());
                        string Yk = new VigenereCipher(testK, alphabet).Decrypt(ciphertext);

                        double[,] D = GetBigramTable(Yk, alphabet);

                        double w = W(E, D);

                        if (w < sw)
                        {
                            sw = w;
                            k = testK;
                        }
                    }
                }

            } while (k == k0);

            Console.WriteLine("Искомый ключ: " + k);

            File.WriteAllText(ansKeyFile, k);
        }



        static void EncryptText(string textFile, string ciphertextFile, string keyFile, string alphabetFile)
        {
            string text = Regex.Replace(File.ReadAllText(textFile), @"[\p{P}\r\n]", "").ToLower();
            string ciphertext = new VigenereCipher(File.ReadAllText(keyFile), File.ReadAllText(alphabetFile)).Encrypt(text);

            File.WriteAllText(ciphertextFile, ciphertext);
            Console.WriteLine("Шифрование текста завершено");
        }

        static void DecryptText(string ciphertextFile, string decryptedTextFile, string keyFile, string alphabetFile)
        {
            string ciphertext = File.ReadAllText(ciphertextFile);
            string decryptedText = new VigenereCipher(File.ReadAllText(keyFile), File.ReadAllText(alphabetFile)).Decrypt(ciphertext);

            File.WriteAllText(decryptedTextFile, decryptedText);
            Console.WriteLine("Дешифрование текста завершено.\n");
        }

        static string DecryptText(string ciphertextFile, string key, string alphabetFile)
        {
            string ciphertext = File.ReadAllText(ciphertextFile);
            string decryptedText = new VigenereCipher(key, File.ReadAllText(alphabetFile)).Decrypt(ciphertext);

            return decryptedText;
        }
    }
}
