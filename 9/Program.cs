﻿using System;
using System.Numerics;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _9
{
    class Program
    {
        static string nFile = @"..\..\files\n.txt";
        static string cFile = @"..\..\files\c.txt";
        static string pFile = @"..\..\files\p.txt";
        static string fFile = @"..\..\files\f.txt";

        static void Main(string[] args)
        {
            RoPollardMethod(nFile, cFile, pFile, fFile);
        }

        static BigInteger f(BigInteger x, int[] fn)
        {
            BigInteger res = 0;
            for (int i = 0; i < fn.Length; i++)
            {
                res += BigInteger.Pow(x, i) * fn[i];
            }
            return res;
        }

        static void RoPollardMethod(string nFile, string cFile, string pFile, string fFile)
        {
            BigInteger n = BigInteger.Parse(File.ReadAllLines(nFile)[0]);
            BigInteger c = BigInteger.Parse(File.ReadAllLines(cFile)[0]);

            string line = File.ReadAllText(fFile);
            string[] args = line.Split(new char[] { ' ' });
            int[] fn = new int[args.Length];
            for (int i = 0; i < args.Length; i++)
            {
                fn[i] = int.Parse(args[i]);
            }
            Array.Reverse(fn);

            BigInteger a = c;
            BigInteger b = c;

            BigInteger d = 1;
            BigInteger p = d;
            while (d == 1)
            {
                a = (f(a, fn) % n + n) % n;
                b = (f(b, fn) % n + n) % n;
                b = (f(b, fn) % n + n) % n;

                d = BigInteger.GreatestCommonDivisor(BigInteger.Abs(a - b), n);
                p = d;

                if (d == n)
                {
                    File.WriteAllText(pFile, "Делитель не найден");
                    Console.WriteLine("Делитель не найден");
                    return;
                }
            }

            File.WriteAllText(pFile, p.ToString());
            Console.WriteLine("Делитель найден");
        }
    }
}
