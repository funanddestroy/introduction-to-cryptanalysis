﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2
{
    public class VigenereCipher
    {
        private string key;
        private string alphabet;

        public VigenereCipher(string key, string alphabet)
        {
            this.key = (string)key.Clone();
            this.alphabet = (string)alphabet.Clone();
        }

        public string Encrypt(string text)
        {
            StringBuilder ciphertext = new StringBuilder();
            for (int i = 0; i < text.Length; i++)
            {
                ciphertext.Append(alphabet[(alphabet.IndexOf(text[i]) + alphabet.IndexOf(key[i % key.Length])) % alphabet.Length]);
            }

            return ciphertext.ToString();
        }

        public string Decrypt(string ciphertextxt)
        {
            StringBuilder text = new StringBuilder();
            for (int i = 0; i < ciphertextxt.Length; i++)
            {
                text.Append(alphabet[((alphabet.IndexOf(ciphertextxt[i]) - alphabet.IndexOf(key[i % key.Length])) % alphabet.Length + alphabet.Length) % alphabet.Length]);
            }

            return text.ToString();
        }
    }
}
