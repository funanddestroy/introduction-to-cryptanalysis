﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2
{
    class Program
    {
        static void Main(string[] args)
        {
            string coincidenceIndexFile = @"..\..\files\coincidence_index.txt";
            string averageCoincidenceIndexFile = @"..\..\files\average_coincidence_index.txt";
            string vigenereCipherIndexFile = @"..\..\files\vigenere_cipher_index.txt";
            string alphabetFile = @"..\..\files\alphabet.txt";
            string key5 = @"..\..\files\k5.txt";
            string key7 = @"..\..\files\k7.txt";
            string text1File = @"..\..\files\text1.txt";
            string text2File = @"..\..\files\text2.txt";

            string text1 = Regex.Replace(File.ReadAllText(text1File), "[-.?!)(,:\"'}{%@#$^&_+=\\|/><; \r\n]", "").ToLower();
            string text2 = Regex.Replace(File.ReadAllText(text2File), "[-.?!)(,:\"'}{%@#$^&_+=\\|/><; \r\n]", "").ToLower();

            Console.WriteLine("Последовательности считаны успешно.\n");

            Random rand = new Random();            

            Console.WriteLine("1 - Индекс совпадения;\n2 - Средний индекс совпадения;\n3 - Генерация двух случайных последовательностей;\n4 - Шифрование шифром Виженера ключами длины 5 и 7 и вычисление индексов совпадений текстов и шифротекстов при сдвиге.\n");

            string command = Console.ReadLine();
            switch (command)
            {
                case "1":
                    double ci = Index.CoincidenceIndex(text1, text2);
                    Console.WriteLine("Индекс совпадения: " + ci);
                    File.WriteAllText(coincidenceIndexFile, ci.ToString());
                    break;

                case "2":
                    double aci = Index.AverageCoincidenceIndex(text1, text2);
                    Console.WriteLine("Средний индекс совпадения: " + aci);
                    File.WriteAllText(averageCoincidenceIndexFile, aci.ToString());
                    break;

                case "3":
                    Console.Write("Введите длину последовательностей: ");
                    int length = int.Parse(Console.ReadLine());

                    string randomText1 = RandomString(length, alphabetFile, rand);
                    string randomText2 = RandomString(length, alphabetFile, rand);

                    File.WriteAllText(text1File, randomText1);
                    File.WriteAllText(text2File, randomText2);

                    int idx = 1;
                    while (File.Exists(@"..\..\files\archive_random_" + idx + ".txt"))
                    {
                        idx++;
                    }

                    File.WriteAllText(@"..\..\files\archive_random_" + idx + ".txt", randomText1);
                    File.WriteAllText(@"..\..\files\archive_random_" + (++idx) + ".txt", randomText2);

                    Console.WriteLine("Последовательности сгенерированы.");
                    break;

                case "4":
                    int dist = 20;
                    StreamWriter writerV = new StreamWriter(new FileStream(vigenereCipherIndexFile, FileMode.Create));

                    string ciphertextK5 = new VigenereCipher(File.ReadAllText(key5), File.ReadAllText(alphabetFile)).Encrypt(text1);
                    string ciphertextK7 = new VigenereCipher(File.ReadAllText(key7), File.ReadAllText(alphabetFile)).Encrypt(text1);

                    Console.Write("Введите количество сдвигов: ");
                    int shiftCount = int.Parse(Console.ReadLine());

                    writerV.WriteLine(string.Format("     {0," + dist + "}{1," + dist + "}{2," + dist + "}", "text", "k = 5", "k = 7"));
                    Console.WriteLine(string.Format("     {0," + dist + "}{1," + dist + "}{2," + dist + "}", "text", "k = 5", "k = 7"));

                    for (int i = 0; i < 15; i++)
                    {
                        double v = Index.CoincidenceIndex(text1, ShiftText(text1, i + 1));
                        double v5 = Index.CoincidenceIndex(ciphertextK5, ShiftText(ciphertextK5, i + 1));
                        double v7 = Index.CoincidenceIndex(ciphertextK7, ShiftText(ciphertextK7, i + 1));
                        writerV.WriteLine(string.Format("{0,5}{1," + dist + "}{2," + dist + "}{3," + dist + "}", (i + 1), v, v5, v7));
                        Console.WriteLine(string.Format("{0,5}{1," + dist + "}{2," + dist + "}{3," + dist + "}", (i + 1), v, v5, v7));
                    }

                    writerV.Close();
                    break;
            }
        }

        public static string RandomString(int length, string alphabetFile, Random rand)
        {
            string alphabet = File.ReadAllText(alphabetFile);
            
            StringBuilder randomString = new StringBuilder();
            for (int i = 0; i < length; i++)
            {
                randomString.Append(alphabet[rand.Next(alphabet.Length)]);
            }

            return randomString.ToString();
        }

        public static string ShiftText(string text, int l)
        {
            return text.Substring(l, text.Length - l);
        }
    }
}
