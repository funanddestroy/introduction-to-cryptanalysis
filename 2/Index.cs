﻿using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2
{
    public class Index
    {
        public static double CoincidenceIndex(string firstText, string secondText)
        {
            int N = Math.Min(firstText.Length, secondText.Length);
            double result = 0;
            for (int i = 0; i < N; i++)
            {
                if (firstText[i] == secondText[i])
                {
                    result++;
                }
            }

            return (result / N) * 100;
        }

        public static double AverageCoincidenceIndex(string firstText, string secondText)
        {
            int N = firstText.Length;
            int N1 = secondText.Length;

            string firstTmp = (string)firstText.Clone();
            string secondTmp = (string)secondText.Clone();

            double result = 0;

            while (firstTmp.Length != 0 || secondTmp.Length != 0)
            {
                char c = firstTmp.Length != 0 ? firstTmp[0] : secondTmp[0];

                double py = new Regex(c.ToString()).Matches(firstTmp).Count;
                double pz = new Regex(c.ToString()).Matches(secondTmp).Count;

                result += (py / N) * (pz / N1);

                firstTmp = Regex.Replace(firstTmp, "[" + c + "]", "");
                secondTmp = Regex.Replace(secondTmp, "[" + c + "]", "");
            }

            return  result * 100;
        }
    }
}
