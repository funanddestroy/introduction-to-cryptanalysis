﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;

namespace _10
{
    class Program
    {
        static string nFile = @"..\..\files\n.txt";
        static string pFile = @"..\..\files\p.txt";
        static string sFile = @"..\..\files\s.txt";
        static string ansFile = @"..\..\files\ans.txt";

        static void Main(string[] args)
        {
            BigInteger n = BigInteger.Parse(File.ReadAllLines(nFile)[0]);
            int s = int.Parse(File.ReadAllLines(sFile)[0]);

            string[] lines = File.ReadAllLines(pFile);
            BigInteger[] p = new BigInteger[lines.Length];
            for (int i = 0; i < lines.Length; i++)
            {
                p[i] = BigInteger.Parse(lines[i]);
            }

            BigInteger a = Random(2, n - 2, new Random());

            int l = 0;
            for (int i = 0; i < s; i++)
            {
                l = (int)(double.Parse(BigInteger.Log(n).ToString()) / double.Parse(BigInteger.Log(p[i]).ToString()));
                a = BigInteger.ModPow(a, BigInteger.Pow(p[i], l), n);
            }

            BigInteger d = BigInteger.GreatestCommonDivisor(a - 1, n);

            if (d == 1 || d == n)
            {
                File.WriteAllText(ansFile, "Делитель не найден");
                return;
            }

            File.WriteAllText(ansFile, d.ToString());
        }

        static BigInteger Random(BigInteger min, BigInteger max, Random rand)
        {
            string str = "";
            for (int i = 0; i < max.ToString().Length; i++)
            {
                str += rand.Next() % 10;
            }
            BigInteger tmp = BigInteger.Parse(str) % max;
            if (tmp < min)
            {
                tmp = min;
            }

            return tmp;
        }
    }
}
